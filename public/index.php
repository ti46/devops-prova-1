<?php


require_once __DIR__ . '/../vendor/autoload.php';

use Mova\DevOpsExample\Service\TimeFormatter;

$server = new Swoole\HTTP\Server('0.0.0.0', 9501);
$server->set([
    'worker_num' => 4,
    'backlog' => 128,
]);

$time = new TimeFormatter();

// The main HTTP server request callback event, entry point for all incoming HTTP requests
$server->on('Request', function (Swoole\Http\Request $request, Swoole\Http\Response $response) use ($server, $time) {
    $timezone = $request->get['timezone'] ?? '';

    try {
        $dtz = new DateTimeZone($timezone);
        $resultString = $time->inTimezone(new DateTime(), $dtz);
    } catch (Throwable $e) {
        $response->status(400);
        $resultString = sprintf('Invalid timezone', $timezone);
    }

    $respContent = json_encode([
        'worker_id' => $server->getWorkerId(),
        'timezone' => $timezone,
        'time' => $resultString,
    ], JSON_UNESCAPED_SLASHES);

    $response->header('Content-Type', 'application/json');
    $response->end($respContent);
});

$server->start();
